package com.forcadevenda.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import com.forcadevenda.sinc.SincXML;
import com.forcadevenda.uteis.G;
import com.forcadevenda.uteis.GApi;

public class Sincronizar extends Activity{
    
    private CheckBox cbProdutos;
    private CheckBox cbClientes;
    private CheckBox cbCondPagto;
    private CheckBox cbGastos;
    private CheckBox cbPedidos;
    
    private Button btProcessar;
    
    private ProgressDialog dialog;
    private Handler handler = new Handler();
    
    public static void call(Activity act) {
        try {
            Intent i = new Intent(act, Sincronizar.class);

            act.startActivity(i);
        } catch (Exception e) {
            G.msgErro(act, G.getString(act, R.string.erroformexibir), e.getMessage());
        }
    }    
    
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.sincronizar);
        
        inicializar();              
    }

    private void inicializar() {
        G.setPreferencias(this);
        
        cbProdutos = (CheckBox)findViewById(R.id.cbProdutos);        
        cbClientes = (CheckBox)findViewById(R.id.cbClientes);        
        cbCondPagto = (CheckBox)findViewById(R.id.cbCondPagto);        
        cbGastos = (CheckBox)findViewById(R.id.cbGastos);        
        cbPedidos = (CheckBox)findViewById(R.id.cbPedidos);                
        
        marcarTodos(true);
        
        btProcessar = (Button)findViewById(R.id.btProcessar);
        
        btProcessar.setOnClickListener(new OnClickListener() {
            public void onClick(View arg0) {
                dialog = ProgressDialog.show(Sincronizar.this, G.getString(Sincronizar.this, R.string.titmsgsincronizando), G.getString(Sincronizar.this, R.string.msgaguardar), false, true);
                processar();
            }
        });
    }
    
    private void processar(){
        new Thread(){
            
            @Override
            public void run(){
                boolean erros = false;        

                if ((!cbProdutos.isChecked()) &&
                    (!cbClientes.isChecked()) &&
                    (!cbCondPagto.isChecked()) &&
                    (!cbGastos.isChecked()) &&
                    (!cbPedidos.isChecked())){
                    G.msgInformacao(Sincronizar.this, G.getString(Sincronizar.this, R.string.nadaselecionado));
                    return;
                }

                SincXML sync = new SincXML(Sincronizar.this);                        

                if (cbClientes.isChecked()){
                    erros = (erros || !sync.expClientes());
                    erros = (erros || !sync.impClientes());
                }

                if (cbProdutos.isChecked())
                    erros = (erros || !sync.impProdutos());

                if (cbCondPagto.isChecked())
                    erros = (erros || !sync.impCondPagtos());

                if (cbGastos.isChecked())
                    erros = (erros || !sync.expGastos());

                if (cbPedidos.isChecked())
                    erros = (erros || !sync.expOrcamentos());

                atualizaTela(!erros);            
            }}.start();
    }
    
    private void atualizaTela(final boolean ok){
        handler.post(new Runnable(){
            @Override
            public void run(){
                dialog.dismiss();
                
                if(ok)
                    G.alertar(Sincronizar.this, G.getString(Sincronizar.this, R.string.sucesso));                                    
                else
                    G.alertar(Sincronizar.this, G.getString(Sincronizar.this, R.string.sucessocomerro));
                
                GApi.enviarEmail(Sincronizar.this, new String[]{G.prefEmailExport}, getString(R.string.emailassuntoexport) + G.strEntreParenteses(G.prefCodVend), getString(R.string.emailmensagemexport), new String[]{
                    G.getPathSdCard() + G.strEntreChars(G.DIRBASE, "/") + G.strEntreChars(G.DIRXML, "/") + SincXML.arqXmlExpGastos, 
                    G.getPathSdCard() + G.strEntreChars(G.DIRBASE, "/") + G.strEntreChars(G.DIRXML, "/") + SincXML.arqXmlExpClientes,
                    G.getPathSdCard() + G.strEntreChars(G.DIRBASE, "/") + G.strEntreChars(G.DIRXML, "/") + SincXML.arqXmlExpOrcamentos});
            }
        });
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
	menu.add(0, G.IDTODOS,  0, R.string.menutodos).setIcon(R.drawable.todos);
	menu.add(0, G.IDNENHUM, 0, R.string.menunenhum).setIcon(R.drawable.nenhum);
	return true;
    }          
    
    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {    
        switch (item.getItemId()) {
            case G.IDTODOS: marcarTodos(true); break;
            case G.IDNENHUM: marcarTodos(false); break;            
        }
        return true;
    }

    private void marcarTodos(boolean valor) {
        cbProdutos.setChecked(valor);
        cbClientes.setChecked(valor);
        cbCondPagto.setChecked(valor);
        cbGastos.setChecked(valor);
        cbPedidos.setChecked(valor);        
    }
    
}
