package com.forcadevenda.classes;

public abstract class GRegistroImport extends GRegistro{

    protected String codigoImport;
    
    public static final String colCodigoImport = "CODIGOIMPORT";

    public String getCodigoImport() {
        return codigoImport;
    }

    public void setCodigoImport(String codigoImport) {
        this.codigoImport = codigoImport;
    }
    
}
